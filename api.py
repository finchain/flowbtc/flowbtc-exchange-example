#!/usr/bin/env python

# WS client example

import asyncio
import json
import websockets

sequenceNumber = 0
password = "seu password"
username = "seu usuario"
accountId = "seu accountId"

def getDataFromResp(data):
    return json.loads(json.loads(data)['o'])

def mountFrame(functionName,payload):
    global sequenceNumber
    frame = {
        "m": 0,
        "i": sequenceNumber,
        "n": functionName,
        "o": json.dumps(payload)
    }
    sequenceNumber += 2
    return json.dumps(frame)
    
async def authenticate(websocket):
    global username
    global password
    #Logar
    await websocket.send(mountFrame("WebAuthenticateUser",{
        "UserName": username,
        "Password": password
    }))

    resp = await websocket.recv()
    print(f"< {resp}")

async def sendOrder(websocket):
    #SendOrder
    # InstrumentId - (1) BTC/BRL (2)ETH/BRL (3) LTC/BRL (5) BCH/BRL (6) ETH/BTC (7)XRP/BRL (8) EOS/BRL
    # Side (0) Compra (1) Venda
    # OrderType (1) Mercado (2) Limite (3) Stop
    global accountId
    orderPayload = {
        "OMSId": 1,
        "InstrumentId": 7,
        "AccountId": accountId,
        "Quantity": 1,
        "Side": 0,
        "TimeInForce": 0,
        "OrderType": 2,
        "LimitPrice": 0.5
    }
    await websocket.send(mountFrame("SendOrder",orderPayload))
    resp = await websocket.recv()
    print(f"< {resp}")
    orderId = getDataFromResp(resp)['OrderId']
    return orderId

async def cancelOrder(websocket, orderId):
    cancelOrderPayload = {
        "OMSId": 1,
        "AccountId": 33194,
        "OrderId": orderId
    }
    await websocket.send(mountFrame("CancelOrder",cancelOrderPayload))
    resp = await websocket.recv()
    print(f"< {resp}")

def parseBook(book):
    parsedBook = {
        "BuySide": {},
        "SellSide": {}
    }
    for item in book:
        price = str(item[6])
        if(item[9] == 0):
            parsedBook["BuySide"][price] = parseBookItem(item)
        else:
            parsedBook["SellSide"][price] = parseBookItem(item)
    return parsedBook

def parseBookItem(item):
    return {
        "Price": item[6],
        "Quantity": item[8],
        "Side": item[9]
    }

def updateBook(book,newItems):
    print(f"update book {newItems}")
    for item in newItems:
        print("olá")
        currentBook = book["BuySide"]
        side = "BuySide" if item[9] == 0 else "SellSide"
        if(item[9] == 1):
            currentBook = book["SellSide"]
        price = str(item[6])
        if(item[3] == 2):
            if( price in currentBook):
                print(f"deletando ordem {price} do book {side}")
                del currentBook[price]
        else:
           print(f"adicionando ordem {price} do {side}")
           currentBook[price] = parseBookItem(item)
        book['BuySide'] = currentBook if item[9] == 0 else book['BuySide']
        book['SellSide'] = currentBook if item[9] == 1 else book['SellSide']
    return book

async def orderBook(websocket):
    subscribeLevel2payload = {
        "OMSId": 1,
        "InstrumentId": 1,
        "Depth": 12
    }
    await websocket.send(mountFrame("SubscribeLevel2",subscribeLevel2payload))
    count = 0
    #Obs: Quando você envia um subscribe level2 ele devolve uma foto do book, volta com o "n"da resposta SubscribeLevel2
    # após isso começa a chegar atualizações do book que volta como Level2UpdateEvent e diz o que alterar na "foto" que você já tem.
    # Isso ocorre pela 4 posição do array onde 0 - novo item, 1 - update e 2- delete. (olhar o metódo updateBook)
    resp = await websocket.recv()
    book = parseBook(getDataFromResp(resp))
    print(f"< Foto book inicial = {json.dumps(book, indent=4, sort_keys=True)} \n")
    while(count != 5):
        resp = await websocket.recv()
        print(f"< {resp}")
        book = updateBook(book,getDataFromResp(resp))
        count += 1
    unSubscribeLevel2payload = {
        "OMSId": 1,
        "InstrumentId": 1,
    }
    #Dando unsubscribe nos eventos do book
    await websocket.send(mountFrame("UnsubscribeLevel2",unSubscribeLevel2payload))
    resp = await websocket.recv()
    print(f"< {resp}")
    print(f"Book final => {json.dumps(book, indent=4, sort_keys=True)}")

async def ws():
    async with websockets.connect(
            'wss://api.flowbtc.com.br/WSGateway/') as websocket:

        #Auth
        print("####Auth###")
        await authenticate(websocket)
        print("################")
        #SendOrder
        print("\n\n###Send Order###")
        orderId = await sendOrder(websocket)
        print("################")

        #CancelOrder
        print("\n\n###Cancel Order###")
        await cancelOrder(websocket,orderId)
        print("################")

        #orderBook
        print("\n\n###Order Book###")
        await orderBook(websocket)
        print("################")

asyncio.get_event_loop().run_until_complete(ws())